#!/bin/bash
if [[ $# == "0" ]]; then
  echo "Usage: `basename "$0"` [export|import]"
else
  if [[ $1 == "export" ]]; then
    if [[ ! -d keyspace ]]; then
      mkdir keyspace
    fi
    if [[ ! -d data ]]; then
      mkdir data
    fi
    cqlsh -e 'DESCRIBE schema' > schema.cql
    while read line
      do
        cqlsh -e "DESCRIBE KEYSPACE $line" > keyspace/$line.cql
        tables=$(grep "CREATE TABLE" keyspace/$line.cql | awk '{print $3}')
        for table in $tables; do
          #echo "COPY $table to 'data/$table.cql'"
          cqlsh -e "COPY $table to 'data/$table.cql'"
      done
    done < keyspaces.txt
  elif [[ $1 == "import" ]]; then
    tables=`ls data`
    for table in $tables; do
      table=$(echo $table | cut -d. -f1,2)
      #table=$(echo $table | cut -d. -f1,2)
      #cqlsh -u sre -p `head file.txt` -e "TRUNCATE $table" `hostname -I` #uncomment if you want tables wiped before
      #cqlsh -e "COPY $table from 'data/$table.cql'"
      cqlsh --request-timeout=3600 -e "COPY $table from 'data/$table.cql' WITH MAXBATCHSIZE=1" # USE this if you have large tables in fields
    done
  fi
fi

