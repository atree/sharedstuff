#!/usr/bin/env python3
''' Generate the Java Keystores and Certificates for cassandra encryption '''
import argparse
import random
import string
import os

# Define colors
# https://tinyurl.com/y3jdhwpo
GREEN = '\033[0;32m'
YELLOW = '\033[0;33m'
BLUE = '\033[0;34m'
RED = '\033[0;31m'
RESET = '\033[m'

# Testing making some arguments mutually exclusive
PARSER = argparse.ArgumentParser()
PARSER.add_argument('cert_names', nargs='+',
                    help='Required, One ore more cert names, space delimited. Example: na-prod-shared-p14c eu-prod-audit-p14c etc.')
PARSER.add_argument('-d', '--dryrun', dest='dryrun', action='store_true',
                    help='Optional, don\'t really create certs, just show what it would do.')
ARGS = PARSER.parse_args()

def pass_gen(length=16, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    ''' Generate the random password for the certificate. '''
    return ''.join(random.choice(chars) for _ in range(length))

def cert_gen():
    ''' Run commands to create the certificats for client and internode encryption. '''
    TRUST_PW = pass_gen()
    C_TRUST_PW = pass_gen()
    KEY_PW = pass_gen()
    C_KEY_PW = pass_gen()

    ALIAS = " -alias " + CERT
    FILE = CERT_DIR + "/" + CERT
    PW_COMBO = " -keypass " + KEY_PW + " -storepass " + KEY_PW
    C_PW_COMBO = " -keypass " + C_KEY_PW + " -storepass " + C_KEY_PW
    KS_LDAP_STRING = "CN=" + CERT + ", OU=" + CERT + ", O=Ping Identity, C=US"

    ## Create a root CA certificate and key
    os.popen("openssl req -new -newkey rsa:2048 -x509 -nodes -subj /CN=rootCa/OU=\"Ping Cassandra\"/O=\"Ping Identity\"/C=US/ -keyout " + CERT_DIR + "/rootCa/rootCa.key -out " + CERT_DIR + "/rootCa/rootCa.crt -days 1096").read()
    
    ## Create a server truststore
    os.popen("keytool -importcert -keystore " + FILE + "-truststore.jks -alias rootCa  -file " + CERT_DIR + "/rootCa/rootCa.crt -noprompt -keypass " + TRUST_PW + " -storepass " + TRUST_PW).read()
    os.popen("keytool -importcert -keystore " + FILE + "-client-truststore.jks -alias rootCa  -file " + CERT_DIR + "/rootCa/rootCa.crt -noprompt -keypass " + C_TRUST_PW + " -storepass " + C_TRUST_PW).read()
    ## Generate public/private key pair and keystore for each node
    os.popen("keytool -genkeypair -keyalg RSA" + ALIAS + " -keystore " + FILE + ".jks" + PW_COMBO + " -validity 1095 -keysize 2048 -dname \"" + KS_LDAP_STRING + "\"").read()
    os.popen("keytool -genkeypair -keyalg RSA" + ALIAS + "-client -keystore " + FILE + "-client.jks" + C_PW_COMBO + " -validity 1095 -keysize 2048 -dname \"" + KS_LDAP_STRING + "\"").read()
   
    ## Export certificate signing request (CSR) for each node
    os.popen("keytool -certreq" + ALIAS + " -keystore " + FILE + ".jks" + " -file " + FILE + ".csr" + PW_COMBO + " -dname \"" + KS_LDAP_STRING + "\"").read()
    os.popen("keytool -certreq" + ALIAS + "-client -keystore " + FILE + "-client.jks" + " -file " + FILE + "-client.csr" + C_PW_COMBO + " -dname \"" + KS_LDAP_STRING + "\"").read()
    
    ## Sign node certificate with rootCa for each node
    os.popen("openssl x509 -req -CA " + CERT_DIR + "/rootCa/rootCa.crt -CAkey " + CERT_DIR + "/rootCa/rootCa.key -in " + FILE + ".csr -out " + FILE + ".crt_signed -days 1095 -CAcreateserial -passin pass:" + KEY_PW).read()
    os.popen("openssl x509 -req -CA " + CERT_DIR + "/rootCa/rootCa.crt -CAkey " + CERT_DIR + "/rootCa/rootCa.key -in " + FILE + "-client.csr -out " + FILE + "-client.crt_signed -days 1095 -CAcreateserial -passin pass:" + C_KEY_PW).read()
    
    ## Import rootCa certificate to each node keystore
    os.popen("keytool -importcert -alias rootCa -keystore " + FILE + ".jks -file " + CERT_DIR + "/rootCa/rootCa.crt -noprompt " + PW_COMBO).read()
    os.popen("keytool -importcert -alias rootCA -keystore " + FILE + "-client.jks -file " + CERT_DIR + "/rootCa/rootCa.crt -noprompt " + C_PW_COMBO).read()
    
    ## Import node's signed certificate into node keystore for each node
    os.popen("keytool -importcert " + ALIAS + " -keystore " + FILE + ".jks -file " + FILE + ".crt_signed -noprompt " + PW_COMBO).read()
    os.popen("keytool -importcert " + ALIAS + "-client -keystore " + FILE + "-client.jks -file " + FILE + "-client.crt_signed -noprompt " + C_PW_COMBO).read()

    print(GREEN + "---=== " + CERT + " ===---" + RESET)
    print(GREEN + "Truststore: " + TRUST_PW + RESET)
    print(GREEN + "Keystore: " + KEY_PW + RESET)
    print(GREEN + "Client truststore: " + C_TRUST_PW + RESET)
    print(GREEN + "Client keystore: " + C_KEY_PW + RESET)
    print(GREEN + "  Copy " + CERT_DIR + "/*.jks to /etc/cassandra/ on all nodes, or to salt/puppet" + RESET)
    print(GREEN + "  Use passwords above in /etc/cassandra/cassandra.yaml" + RESET)

for CERT in ARGS.cert_names:
    CERT_DIR = "certificates/" + CERT
    if not os.path.exists(CERT_DIR) and not ARGS.dryrun:
        os.makedirs(CERT_DIR + "/rootCa")
        cert_gen()
    elif not os.path.exists(CERT_DIR) and ARGS.dryrun:
        print(YELLOW + "Creating cert directory: %s" % (CERT_DIR) + RESET)
        cert_gen()
    elif os.path.exists(CERT_DIR) and ARGS.dryrun:
        print(YELLOW + "%s already exists.  Rename it or use another name." % (CERT_DIR) + RESET)
    else:
        print(RED + "%s alredy exists.  Rename it or use another name." % (CERT_DIR) + RESET)
