#!/usr/bin/env python3
#import cassandra.policies as policies
#import cassandra.auth as auth 
from cassandra.cluster import Cluster
from cassandra.policies import DCAwareRoundRobinPolicy
from cassandra.auth import PlainTextAuthProvider

auth_provider = PlainTextAuthProvider(
        username='outage_test_role', password='M53pbebB9Unpw6Sh')

cluster = Cluster(
    ['10.77.13.46', '10.77.19.40', '10.77.25.189'],
        auth_provider=auth_provider,
        load_balancing_policy=DCAwareRoundRobinPolicy(local_dc='us-east-2'),
        port=9042)

session = cluster.connect('reaper_db')

if session:
    print("Good")
else:
    print("Bad")
