#!/usr/bin/env python3
''' Use this only for manual repair.  This shouldn't be scheduled. '''
import argparse
import os
import re
import getpass

# Define colors
# https://tinyurl.com/y3jdhwpo
GREEN = '\033[0;32m'
YELLOW = '\033[0;33m'
BLUE = '\033[0;34m'
RED = '\033[0;31m'
RESET = '\033[m'

# Testing making some arguments mutually exclusive
PARSER = argparse.ArgumentParser()
PARSER.add_argument('-a', '--auth', dest='auth', action='store_true',
                    help='Optional, prompts for jmx username and password.')
PARSER.add_argument('-d', '--dryrun', dest='dryrun', action='store_true',
                    help='Optional, print what would be done without doing anything.')
PARSER.add_argument('-k', '--keyspace', dest='keyspace',
                    help='Optional, specify keyspace to repair. Required if defining a table')
PARSER.add_argument('-t', '--table', dest='table',
                    help='Optional, specify table to repair.  Requires a keyspace to be defined')
ARGS = PARSER.parse_args()

def clusterInfo():
    '''Connect to the cluster and get the version and list of nodes in the cluster'''
    COMMAND = "nodetool %s version;  nodetool %s describecluster; nodetool %s tpstats | grep Repair#" % (ARGS.auth, ARGS.auth, ARGS.auth)
    CLUSTER_INFO = os.popen(COMMAND).read()
    if CLUSTER_INFO:
        print("INFO: " + CLUSTER_INFO)
        VERSION = re.findall(r"ReleaseVersion: .*$", CLUSTER_INFO, re.MULTILINE)[0].split()[1]
        SCHEMA = re.findall(r"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}", CLUSTER_INFO, re.MULTILINE)
        UNREACHABLE = re.findall(r"UNREACHABLE: .*$", CLUSTER_INFO, re.MULTILINE)
        NODES = re.findall(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", CLUSTER_INFO, re.MULTILINE)

        if ARGS.dryrun:
            print(YELLOW + COMMAND + RESET)

        if len(SCHEMA) > 1 or len(UNREACHABLE) > 0:
            print(RED + "There is more than one schema or there are unreachable nodes." + RESET)
        else:
            print(GREEN + "Schema count is good and there are no unreachable nodes." + RESET)
            runRepair(VERSION, NODES)
    else:
        print(CLUSTER_INFO)

def runRepair(VERSION, NODES):
    '''Pause running reaper repairs, repair the cluster, then resume the reaper repairs'''
    #REPAIR_GREP = " | grep --color=always 'Repair command #[0-9]* finished\|Starting repair command #[0-9]*'"
    REPAIR_GREP = " | grep --color=always 'Starting\\|successfully\\|finished in\\|failed'"

    if VERSION.startswith('3'):
        print(GREEN + "Pausing reaper jobs..." + RESET)
        REAPER_RUN = os.popen("curl -s http://localhost:8080/repair_run | grep -o -P '.{0,36}\",\"state\":\"RUNNING\"' | cut -c1-36").read().splitlines()
        for JOB in REAPER_RUN:
            if ARGS.dryrun:
                print(YELLOW + "curl -s -XPUT http://localhost:8080/repair_run/" + JOB + "/state/PAUSED 2>&1" + RESET)
            else:
                print(GREEN + "Pausing reaper job: " + JOB + RESET)
                os.system("curl -s -XPUT http://localhost:8080/repair_run/" + JOB + "/state/PAUSED 2>&1")

        NUM_NODES = str(len(NODES))
        NUM_THIS = 1
        print(GREEN + "Repairing " + NUM_NODES + " nodes." + RESET)
        for NODE in NODES:
            COMMAND = "nodetool -h %s %s repair -full -j 4 %s %s %s" % (NODE, ARGS.auth, ARGS.keyspace, ARGS.table, REPAIR_GREP)
            if ARGS.dryrun:
                print(YELLOW + COMMAND + RESET)
            else:
                print(GREEN + "### " + NODE + ": " +  str(NUM_THIS) + " of " + NUM_NODES + " ###" + RESET)
                os.system(COMMAND)
            NUM_THIS = NUM_THIS + 1

        print(GREEN + "Resuming reaper jobs..." + RESET)
        REAPER_PAU = os.popen("curl -s http://localhost:8080/repair_run | grep -o -P '.{0,36}\",\"state\":\"PAUSED\"' | cut -c1-36").read().splitlines()
        for JOB in REAPER_PAU:
            if ARGS.dryrun:
                print(YELLOW + "curl -s -XPUT http://localhost:8080/repair_run/" + JOB + "/state/RUNNING 2>&1" + RESET)
            else:
                print(GREEN + "Pausing reaper job: " + JOB + RESET)
                os.system("curl -s -XPUT http://localhost:8080/repair_run/" + JOB + "/state/RUNNING 2>&1")

    elif VERSION.startswith('2'):
        print(GREEN + "Repairing " + str(len(NODES)) + " nodes." + RESET)
        for NODE in NODES:
            COMMAND = "nodetool -h %s %s repair -pr %s %s %s" % (NODE, ARGS.auth, ARGS.keyspace, ARGS.table, REPAIR_GREP)
            if ARGS.dryrun:
                print(YELLOW + COMMAND + RESET)
            else:
                os.system(COMMAND)

if not ARGS.keyspace and ARGS.table:
    print(RED + "You must define which keyspace (-k, --keyspace) %s is in." % (ARGS.table) + RESET)
else:
    if ARGS.auth:
        if os.path.exists("/etc/cassandra/jmxremote.access") and os.path.exists("/etc/cassandra/jmxremote.password"):
            USER = os.popen("sudo grep readwrite /etc/cassandra/jmxremote.access | cut -d \" \" -f 1").read().strip()
            PASSWD = os.popen("sudo grep " + USER + " /etc/cassandra/jmxremote.password | cut -d \" \" -f 2").read().strip()
            if PASSWD == "None":
                PASSWD = getpass.getpass("Password not found, please enter it: ")
        elif os.path.exists("/etc/cassandra-reaper/cassandra-reaper.yaml"):
            USER = os.popen("grep -A1 'jmxAuth:' /etc/cassandra-reaper/cassandra-reaper.yaml | sed -n -e 's/  username: //p'").read().strip()
            PASSWD = os.popen("grep -A1 'username: " + USER + "' /etc/cassandra-reaper/cassandra-reaper.yaml | sed -n -e 's/  password: //p'").read().strip()
            if PASSWD == "None":
                PASSWD = getpass.getpass("Password not found, please enter it: ")
        else:
            USER = input("JMX Username: ")
            PASSWD = getpass.getpass("JMX Password: ")
        ARGS.auth = str("-u " + USER + " -pw " + PASSWD)
    elif not ARGS.auth:
        ARGS.auth = str("")
    if not ARGS.keyspace:
        ARGS.keyspace = str("")
    if not ARGS.table:
        ARGS.table = str("")
    clusterInfo()
