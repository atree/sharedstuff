#!/bin/bash
# This imports the sstables from snapshots via sstableloader
nodes=$(find ./ -maxdepth 1 -type d -name '10.*')
for node in $nodes; do
  keyspaces=$(ls $node)
  for keyspace in $keyspaces; do
    tables=$(ls $node/$keyspace/)
    for table in $tables; do
      sstableloader -d <comma sep list of nodes> -u sre -pw '<password>' $node/$keyspace/$table/ >> import.log
    done
  done
done

