#!/usr/bin/env python3
# Todo list:
#     use Vault to get username and password
#     get cassandra nodes instead of using variable
''' Collect snapshots from s3 backups and inserts them into this cluster. '''
import argparse
import boto3
import datetime
import getpass
import os

# Define colors
# https://tinyurl.com/y3jdhwpo
GREEN = '\033[0;32m'
YELLOW = '\033[0;33m'
RED = '\033[0;31m'
RESET = '\033[m'

# Date & Time
NOW = datetime.datetime.now()
DATETIME = NOW.strftime("%Y-%m-%d %H:%M:%S")

# Define s3 for use in other functions
s3 = boto3.client("s3")

# Parse arguments
# https://docs.python.org/3/library/argparse.html
PARSER = argparse.ArgumentParser()
PARSER.add_argument('snap_path', help='The path to the snapshots in s3, bucket/cluster/date (without the s3://)')
PARSER.add_argument('-d', '--dryrun', dest='dryrun', action='store_true',
                     help='Optional, print (in yellow) what would be done without doing anything.')
PARSER.add_argument('-l', '--local', dest='local', action='store_true',
                     help='WIP, use a local ccm cluster to test against.')
PARSER.add_argument('-1', '--onenode', dest='onenode', action='store_true',
                     help='WIP, download and restore 1 source node at a time')
ARGS = PARSER.parse_args()

def prep_schema(snap_path):
    ''' Prepares for the recovery by creating the directory structure,
        copying the files, decompressing, and reorganizing the files. '''
    if not os.path.exists('recovery'):
        if ARGS.dryrun:
            print(YELLOW + "os.makedirs('recovery')" + RESET)
        else:
            print(GREEN + "Creating recovery directory" + RESET)
            os.makedirs('recovery')
    if not os.path.exists("./recovery/schema.cql"):
        if ARGS.dryrun:
            print(YELLOW + "s3.download_file(" + bucket + ", " + prefix + "/schema.cql, ./recovery/schema.cql)" + RESET)
        else:
            print(GREEN + "Downloading schema.cql" + RESET)
            s3.download_file(bucket, prefix + "/schema.cql", "./recovery/schema.cql")
    if not os.path.exists("./recovery/schema.cql.bak"):
        if ARGS.dryrun:
            print(YELLOW + "sed -i.bak -e 's/CREATE KEYSPACE/& IF NOT EXISTS/g' -e 's/CREATE TABLE/& IF NOT EXISTS/g' ./recovery/schema.cql" + RESET)
            print(YELLOW + "os.popen(\"cqlsh -u " + cas_user + " -p " + cas_pass + " " + tgt_node + " -e \"source './recovery/schema.cql';\"")
        else:
            print(GREEN + "Editing and importing schema.cql" + RESET)
            os.popen("sed -i.bak -e 's/CREATE KEYSPACE/& IF NOT EXISTS/g' -e 's/CREATE TABLE/& IF NOT EXISTS/g' ./recovery/schema.cql")
            # This cqlsh command should move from here, Need to make it conditional upon having not been run yet
            os.popen("cqlsh -u " + cas_user + " -p " + cas_pass + " " + tgt_node + " -e \"source './recovery/schema.cql';\"").read()
    return True

def get_filePaths():
    paginator = s3.get_paginator('list_objects_v2')
    pages = paginator.paginate(Bucket=bucket, Prefix=prefix)
    paths = []
    for page in pages:
        for obj in page['Contents']:
            source_path = obj['Key']
            if len(source_path.split('/')) == 12:
                paths.append(source_path)
    return(paths)

def getSnapshots():
    paths = get_filePaths()
    load_dirs = []
    for src in paths:
        node = src.split('/')[2]
        keyspace = src.split('/')[7]
        table = src.split('/')[8].split('-')[0]
        load_dir = './recovery/' + node + '/' + keyspace + '/' + table
        filename = src.split('/')[11]
        dest = "./recovery/" + node + '/' + keyspace + '/' + table + '/' + filename
        if keyspace not in [ 'system_traces', 'system_schema', 'system', 'system_distributed' ]:
            if not os.path.exists(dest.rsplit('/', 1)[0]):
                if ARGS.dryrun:
                    print(YELLOW + "os.makedirs(%s)" % (dest.rsplit('/', 1)[0]) + RESET)
                else:
                    print(GREEN + "Creating: %s" % (dest.rsplit('/', 1)[0]) + RESET)
                    os.makedirs(dest.rsplit('/', 1)[0])
            if not os.path.exists(dest) and not os.path.exists(os.path.splitext(dest)[0]):
                if ARGS.dryrun:
                    print(YELLOW + "s3.download_file(%s, %s, %s)" % (bucket, src, dest) + RESET)
                else:
                    print(GREEN + "  Downloading %s" % (dest) + RESET)
                    s3.download_file(bucket, src, dest)
            if os.path.exists(dest) and not os.path.exists(os.path.splitext(dest)[0]):
                if ARGS.dryrun:
                    print(YELLOW + "lzop -xU -p" + dest.rsplit('/', 1)[0] + " " + dest + RESET)
                else:
                    print(GREEN + "  Extracting: %s" % (dest) + RESET)
                    os.popen("lzop -xU -p" + dest.rsplit('/', 1)[0] + " " + dest).read()
            if load_dir not in load_dirs:
                load_dirs.append(load_dir)
    return(load_dirs)

def restore():
    load_dirs = getSnapshots()
    for restore in load_dirs:
        if not ('system_traces' or 'system_schema' or 'system' or 'system_distributed') in restore:
            if ARGS.dryrun:
                print(YELLOW + "sstableloader -d %s -u %s -pw %s %s" % (tgt_node, cas_user, cas_pass, restore) + RESET)
            else:
                print(GREEN + "Importing: %s" % (restore) + RESET)
                os.popen("sstableloader -d " + tgt_node + " -u " + cas_user + " -pw " + cas_pass + " " + restore).read()

bucket,cluster,date = ARGS.snap_path.split('/')
prefix = cluster + '/' + date

if ARGS.local:
    tgt_node = "127.0.0.1"
else:
    tgt_node = input("IP addr of target node: ")

cas_user = input("Cassandra user: ")
cas_pass = getpass.getpass(prompt='Cassandra Password: ')

print(GREEN + "Starting recovery at: %s" % (datetime.datetime.now()) + RESET)
if prep_schema(ARGS.snap_path):
    restore()
print(GREEN + "Finishing recovery at: %s" % (datetime.datetime.now()) + RESET)
