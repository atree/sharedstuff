#!/bin/bash
# pingidentity-cassandra-backups-us-east-2/p14c-shared-02/20190825070050
# snapshot_restore.sh
# Collect snapshots from s3 backups and inserts them into this cluster
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
NC=$(tput sgr0)

if [[ $# == "0" ]]; then
  echo "Usage: $(basename "$0") [prep|import]"
  echo "Navigate to the snapshot you want to restore in S3 and copy the path"
elif [[ $1 == "prep" ]]; then
  read -r -p "Paste the path to the snapshot from S3 (ie: pingidentity.cassandra-backups.us-east-1/sso_auth_ORT/20171224191912): " path
  mkdir -p recovery
  echo "Recovery prep beginning: $(date)" >> ./recovery/recovery.log
  echo "Recovery directory is empty, downloading snapshots from s3..."
  if ! [[ $(dpkg --get-selections | grep awscli) ]]; then
    echo "Installing awscli..."
    apt-get install awscli
  fi
  echo "Syncing up the recovery directory from S3..."
  aws s3 sync s3://"$path" ./recovery/ --quiet --region us-east-2

  if [ -f ./recovery/schema.cql ]; then
    mkdir -p ./recovery/import
    if [[ -z "$(ls -A recovery/import/)" ]]; then
      sourcenodes=$(find ./recovery -maxdepth 1 -type d \( -name '172.*' -o -name '10.*' \))
      tables=$(grep 'CREATE TABLE' ./recovery/schema.cql | awk '{print $3}' | grep -v 'system\.\|system_traces\.\|system_distributed\.\|system_schema\.')
      for sourcenode in $sourcenodes; do
        destnode="${sourcenode/recovery/recovery\/import}"
        echo "$destnode"
        for table in $tables; do
          tablepath="${table//[.]/\/}"
          echo "Extracting $tablepath snapshots to $destnode/$tablepath"
          mkdir -p "$destnode/$tablepath"
          find $sourcenode/var/lib/cassandra/data/$tablepath-*/ -name '*.db.lzo' -exec lzop -x -p"$destnode/$tablepath/" {} \;
          sourcecount=$(find $sourcenode/var/lib/cassandra/data/$tablepath-*/ -name '*.db.lzo' | wc -l)
          destcount=$(find $destnode/$tablepath/ -name '*.db' | wc -l)
          if [[ "$sourcecount" == "$destcount" ]]; then
            echo -e "  ${GREEN}Count is good, removing $tablepath db.lzo files${NC}"
            find $sourcenode/var/lib/cassandra/data/$tablepath-*/ -name '*.db.lzo' -exec rm {} \;
          else
            echo -e "  ${RED}  $tablepath - Source: $sourcecount  Dest: $destcount ${NC}"
          fi
        done
      done
      destdc=$(nodetool -u cassandra -pw cassandra status | sed -n -e 's/Datacenter: //p')
      rackcount=$(nodetool -u cassandra -pw cassandra gossipinfo | grep RACK: | cut -d: -f3 | sort -u | wc -l)
      echo "Modifying schema to import..."
      cp ./recovery/schema.cql ./recovery/import/schema.cql
      sed -i "s/{'class': '\\(SimpleStrategy\\|NetworkTopologyStrategy\\)', '.*': '.*'}/{'class': 'NetworkTopologyStrategy', '$destdc': '$rackcount'}/" ./recovery/import/schema.cql
      sed -i "s/CREATE KEYSPACE/CREATE KEYSPACE IF NOT EXISTS/" ./recovery/import/schema.cql
      sed -i "s/CREATE TABLE/CREATE TABLE IF NOT EXISTS/" ./recovery/import/schema.cql
      sed -i "s/CREATE MATERIALIZED VIEW/CREATE MATERIALIZED VIEW IF NOT EXISTS/" ./recovery/import/schema.cql
    else
      echo "Import directory already contains files..."
    fi
  fi
  echo "Recovery prep complete: $(date)" >> ./recovery/recovery.log
  echo "scroll up and look for red. Check ./recovery/schema.cql."
  echo "Run $(basename "$0") import next"

elif [[ $1 == "import" ]]; then
  read -r -p "Superuser [cassandra]: " user
  user="${user:=cassandra}"
  read -r -s -p "Superuser password [cassandra]: " password
  password="${password:=cassandra}"

  echo "Recovery import beginning: $(date)" | tee ./recovery/recovery.log
  if [ -f ./recovery/import/schema.cql ]; then
    echo "Creating schema..."
    cqlsh -u "$user" -p "$password" "$(hostname -I)" -f './recovery/import/schema.cql'
  else
    echo "Something went wrong, the import schema was not created."
  fi

  targetnodes=$(nodetool -u cassandra -pw cassandra describecluster | awk -F "[" '{print $2}' | awk -F "]" '{print $1}')
  targetnodes=${targetnodes//[[:space:]]/}
  sourcenodes=$(find ./recovery/import -type d \( -name '172.*' -o -name '10.*' \))
  tables=$(grep 'CREATE TABLE' ./recovery/schema.cql | awk '{print $3}' | grep -v 'system\.\|system_traces\.\|system_distributed\.\|system_schema\.')
  for sourcenode in $sourcenodes; do
    for table in $tables; do
      tablepath="${table//[.]/\/}"
      echo "Bulkloading snapshots from $sourcenode/$tablepath" | tee ./recovery/recovery.log
      sstableloader -d "$targetnodes" -u "$user" -pw \"$password\" "$sourcenode/$tablepath/" >> ./recovery/import/import.log
    done
  done
  echo "Recovery import complete: $(date)" >> ./recovery/recovery.log
else
  echo "For now you can only run 'prep' or 'import'"
fi
