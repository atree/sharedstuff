#!/usr/bin/env bash
#
# Use this to do a manual repair  when reaper is not available or you don't want to 
# wait for the next scheduled run.  This shouldn't be scheduled.
#
# This script will connect to the node you run it on and collect the ip address of the 
# nodes in the cluster.  We pause any reaper jobs and resume them after the repair
# becaus it's easier than deleting them. It will loop through the nodes running a full
# repair on each node, eliminating the need to manually run the repair command on each node.
#
# If you pass a keyspace argument, only that keyspace will be repaired, otherwise the script
# will repair all keyspaces.

red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
purple=$(tput setaf 5)
normal=$(tput sgr0)

if [[ $# == "0" ]]; then
  printf "Usage: $0 <keyspace> <table>\n\nTo repair all keyspaces use $0 all, specific keyspace and <table> are optional.\n\n"
else
  if [[ $1 == "all" ]]; then
    keyspace=""
  else
    keyspace="$1"
    table="$2"
  fi
  cluster_info=$(nodetool version; nodetool describecluster)
  version=$(echo "${cluster_info}" | grep ReleaseVersion: | sed -n -e 's/ReleaseVersion:[[:blank:]]//p')
  cluster_schema_vers=$(echo "${cluster_info}" | grep -e '^.*\(.\{8\}-.\{4\}-.\{4\}-.\{4\}-.\{12\}\)' | sort | uniq | wc -l | sed -n -e 's/ *//p')
  nodes=$(echo "${cluster_info}" | grep -A 1 Schema | sed -n -e 's/[[:blank:]]*.\{8\}-.\{4\}-.\{4\}-.\{4\}-.\{12\}: //p' -e 's/^\s*//g' | tr -d '[],')

  if [[ $cluster_schema_vers != 1 ]]; then
    printf "${red}ERROR: There is not 1 schema (${cluster_schema_vers}), check for version mismatch and fix before continuing!${normal}\n\n"
  else
    printf "${green}GOOD: There is 1 schema version, proceeding with repair!${normal}\n\n"
    if [[ $version == '3'* ]]; then
      reaprun=( $(curl -s http://localhost:8080/repair_run | grep -o -P '.{0,36}","state":"RUNNING"' | cut -c1-36) )
      printf "Pausing reaper jobs\n\n"
      for reaper_job in "${reaprun[@]}"; do
        curl -s -XPUT http://localhost:8080/repair_run/${reaper_job}/state/PAUSED 2>&1
      done
    fi
    for node in ${nodes[@]}; do
      echo "${green}### ${node} ###${normal}"
      if [[ $version == '2'* ]]; then
        nodetool -h ${node} repair -pr $keyspace $table | grep --color=always 'Repair command #[0-9]* finished\|Starting repair command #[0-9]*'
      elif [[ $version == '3'* ]]; then
        nodetool -h ${node} repair -full -j 4 $keyspace $table | grep --color=always 'Starting\|successfully\|finished in\|failed'
      fi
    done
    if [[ $version == '3'* ]]; then
      reappau=( $(curl -s http://localhost:8080/repair_run | grep -o -P '.{0,36}","state":"PAUSED"' | cut -c1-36) )
      printf "Resuming reaper jobs\n\n"
      for reaper_job in "${reaprun[@]}"; do
        curl -s -XPUT http://localhost:8080/repair_run/${reaper_job}/state/RUNNING 2>&1
      done
    fi
  fi
fi
