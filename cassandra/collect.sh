#!/bin/bash
# Collect snapshots from this node and put them in the directory
# structure that sstableloader expects
node=$(hostname -I)
mkdir -p keyspace
if [[ -z "$(ls -A keyspace/)" ]]; then
  echo "Keyspace directory is empty, generating cql files..."
  gencql=true
else
  echo "Keyspace directory contains files, skipping cql generation..."
  gencql=false
fi

while read line
  do
  if [[ $gencql == true ]]; then
    cqlsh -e "DESCRIBE KEYSPACE $line" "$(hostname -I)" > "keyspace/$line.cql"
  fi
  tables=$(grep 'CREATE TABLE' "keyspace/$line.cql" | awk '{print $3}')
  for table in $tables; do
    tablepath="${table//[.]/\/}"
    backuppath="${node//[[:space:]]/}/$tablepath"
    mkdir -p "$backuppath"
    echo "Copying files to $backuppath"
    find /var/lib/cassandra/data/$tablepath-*/snapshots/$1/ -type f  -exec cp -r {} $backuppath \;
  done
done < keyspaces.txt

