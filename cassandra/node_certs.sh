#!/bin/bash
# This script generates the Java Keystores and $certs for cassandra servers
date=`date +%Y%m%d`
certs="certs_$date"
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'


if [[ $# == "0" ]]; then
  echo "Usage: `basename "$0"` ip ip ip or `basename "$0"` env env env"
  echo "  For example: `basename "$0"` cas_test cas_ort cas_perf cas_prod"
  echo "  or: `basename "$0"` prod-us-west-2-cassandra-logging test-us-east-1-cassandra-logging"
  echo "  or: `basename "$0"` 172.18.0.1 172.18.0.2 172.18.03 172.18.0.4"
  echo "  Note: 1 truststore will be generated for each run of this script."
else
  # Create directory for each cert and the rootCa in a certs_todays date directory
  mkdir -p $certs/rootCa
  for hostip in "$@"; do
    mkdir -p $certs/$hostip
  done
 
  # Generate a random password
  trustpass=`dd if=/dev/urandom bs=9 count=1 2>/dev/null | base64 | tr '+/' '_-'`
  # Create a root CA certificate and key
  openssl req -new -newkey rsa:2048 -x509 -nodes -subj /CN=rootCa/OU="Ping Cassandra"/O="Ping Identity"/C=US/ -keyout $certs/rootCa/rootCa.key -out $certs/rootCa/rootCa.crt -days 1096
  #openssl req -new -x509 -nodes -subj /CN=rootCa/OU="Ping Cassandra"/O="Ping Identity"/C=US/ -keyout $certs/rootCa/rootCa.key -out $certs/rootCa/rootCa.crt -days 1096
  # Create a server truststore
  keytool -importcert -keystore $certs/truststore.jks -alias rootCa  -file $certs/rootCa/rootCa.crt -noprompt -keypass $trustpass -storepass $trustpass
  # Print the truststore and password
  echo -e "${GREEN}---=== truststore password: $trustpass ===--- ${NC}"

  # Loop through each ip, env, etc. making the certs
  for hostip in "$@"; do
    # Generate a random password
    password=`dd if=/dev/urandom bs=9 count=1 2>/dev/null | base64 | tr '+/' '_-'`
    # Generate public/private key pair and keystore for each node
    keytool -genkeypair -keyalg RSA -alias $hostip -keystore $certs/$hostip/$hostip.jks -storepass $password -keypass $password -validity 1095 -keysize 2048 -dname "CN=$hostip, OU=$hostip, O=Test, C=US"
    # Export certificate signing request (CSR) for each node
    keytool -certreq -keystore $certs/$hostip/$hostip.jks -alias $hostip -file $certs/$hostip/$hostip.csr -keypass $password -storepass $password -dname "CN=$hostip, OU=$hostip, O=Test, C=US"
    # Sign node certificate with rootCa for each node
    openssl x509 -req -CA $certs/rootCa/rootCa.crt -CAkey $certs/rootCa/rootCa.key -in $certs/$hostip/$hostip.csr -out $certs/$hostip/$hostip.crt_signed -days 1095 -CAcreateserial -passin pass:$password
    # Import rootCa certificate to each node keystore
    keytool -importcert -keystore $certs/$hostip/$hostip.jks -alias rootCa  -file $certs/rootCa/rootCa.crt -noprompt  -keypass $password -storepass $password
    # Import node's signed certificate into node keystore for each node
    keytool -importcert -keystore $certs/$hostip/$hostip.jks -alias $hostip -file $certs/$hostip/$hostip.crt_signed -noprompt -keypass $password -storepass $password
    echo -e "${GREEN}---=== $hostip password: $password ===--- ${NC}"
  done
  
  echo "Copy the following files to the appropriate nodes, or location in puppet, salt, vault, etc."
  for hostip in "$@"; do
    echo -e "$hostip:  ${GREEN}$certs/$hostip/$hostip.jks${NC} and ${GREEN}$certs/truststore.jks${NC}"
  done
fi
