#!/usr/local/bin/python3
"""Script to put instance data into db and  use it to do stuff"""
import argparse
import os
import time
import sqlite3
import sys
import getpass
import boto3

# Set username
USERNAME = getpass.getuser()

# Define colors
GREEN = '\033[0;32m'
YELLOW = '\033[0;33m'
BLUE = '\033[0;34m'
RED = '\033[0;31m'
RESET = '\033[m'

# Testing making some arguments mutually exclusive
# Rename group[1|2] to make more sense
PARSER = argparse.ArgumentParser()
GROUP1 = PARSER.add_mutually_exclusive_group()
GROUP2 = PARSER.add_mutually_exclusive_group()
PARSER.add_argument('-d', '--dryrun', dest='dryrun', action='store_true',
                    help='Optional, print what would be done without doing anything.')
PARSER.add_argument('-s', '--scan', dest='scan', action='store_true',
                    help='Optional, force a rescan of aws and rebuild the db')
PARSER.add_argument('-e', '--env', dest='environment',
                    help='Optional, specify environment to run for.')
PARSER.add_argument('-n', '--name', dest='name',
                    help='Optional, search for part of an instance name')
PARSER.add_argument('-f', '--file', dest='file',
                    help='Optional, scp this file to the returned hosts')
PARSER.add_argument('-o', '--out', dest='out',
                    help='Optional, specifies the output file.  Default is stdout')
GROUP1.add_argument('-c', '--command', dest='command',
                    help='Optional, run this command on the returned hosts.')
GROUP1.add_argument('-x', '--execute', dest='execute', action='store_true',
                    help='Optional, execute the uploaded file (-f), will pass env and name arguments')
GROUP2.add_argument('-r', '--region', dest='region',
                    help='Optional, specify which region to run for.')
GROUP2.add_argument('-i', '--inverse', dest='inverse', action='store_true',
                    help='Optional, scan regions we don\'t use and alert if any nodes are found')
ARGS = PARSER.parse_args()

# Make connection to the database file
DB_FILE = sys.argv[0].replace("py", "db")
DB_CONNECT = sqlite3.connect(DB_FILE)
DB = DB_CONNECT.cursor()

def scaninstances():
    """Scan the region(s) specified and insert data into db"""
    print(GREEN + "Creating tables. . ." + RESET)
    DB.execute("DROP TABLE IF EXISTS instance")
    DB.execute("DROP TABLE IF EXISTS weirdo")
    DB.execute("CREATE TABLE instance (ip, name, environment, region)")
    DB.execute("CREATE TABLE weirdo (ip, name, environment, region)")

    regions = ['ap-southeast-2', 'eu-central-1', 'eu-west-1', 'us-east-1', 'us-east-2', 'us-west-2']
    no_regions = ['eu-north-1', 'ap-south-1', 'eu-west-3', 'eu-west-2', 'ap-northeast-2',
                  'ap-northeast-1', 'sa-east-1', 'ca-central-1', 'ap-southeast-1', 'us-west-1']

    if ARGS.region and not ARGS.inverse:
        regions = [ARGS.region]
    elif ARGS.inverse and not ARGS.region:
        regions = no_regions

    for region in regions:
        print(GREEN + "Inserting values for %s" % (region) + RESET)
        client = boto3.client('ec2', region_name=region)
        response = client.describe_instances(
            Filters=[
                {'Name': 'instance-state-name', 'Values': ['running'],},
            ],
        )

        for reservation in response['Reservations']:
            for instance in reservation['Instances']:
                privip = instance['PrivateIpAddress']
                environment, name = None, None
                if 'Tags' in instance:
                    for tag in instance['Tags']:
                        if tag['Key'] == 'environment' or tag['Key'] == 'PingEnvironment':
                            environment = tag['Value'][0:4]
                        if tag['Key'] == 'Name':
                            name = tag['Value']
                if (environment and ARGS.environment and environment == ARGS.environment) or (environment and not ARGS.environment):
                    DB.execute('insert into instance values (?, ?, ?, ?)',
                               (privip, name, environment, region))
                else:
                    DB.execute('insert into weirdo values (?, ?, ?, ?)',
                               (privip, name, environment, region))
                DB_CONNECT.commit()

def runscript():
    """Loop through the requested instances and do stuff"""
    query = "SELECT * from instance"
    if ARGS.environment and ARGS.region and ARGS.name:
        query += " where environment='" + ARGS.environment + "'" + " and region='" + ARGS.region + "'" + " and name like '%" + ARGS.name + "%'"
    elif ARGS.environment and ARGS.region and not ARGS.name:
        query += " where environment='" + ARGS.environment + "'" + " and region='" + ARGS.region + "'"
    elif ARGS.environment and not ARGS.region and ARGS.name:
        query += " where environment='" + ARGS.environment + "'" + " and name like '%" + ARGS.name + "%'"
    elif ARGS.environment and not ARGS.region and not ARGS.name:
        query += " where environment='" + ARGS.environment + "'"
    elif not ARGS.environment and ARGS.region and ARGS.name:
        query += " where region='" + ARGS.region + "'" + " and name like '%" + ARGS.name + "%'"
    elif not ARGS.environment and ARGS.region and not ARGS.name:
        query += " where region='" + ARGS.region + "'"
    elif not ARGS.environment and not ARGS.region and ARGS.name:
        query += " where name like '%" + ARGS.name + "%'"

    DB.execute(query)
    rows = DB.fetchall()

    if ARGS.out:
        print(GREEN + "Writing output to %s." % (ARGS.out) + RESET)
        OUTFILE = open(ARGS.out, "a")

    for row in rows:
        privip = row[0]
        name = row[1]
        env = row[2]
        region = row[3]
        if ARGS.dryrun:
            if ARGS.out:
                OUTFILE.write("--- %s - %s ---\n" % (name, privip))
            else:
                print(BLUE + "--- %s - %s ---" % (name, privip) + RESET)
            if ARGS.file:
                if ARGS.out:
                    OUTFILE.write("  scp -pr %s %s@%s:~/\n" % (ARGS.file, USERNAME, privip))
                else:
                    print(YELLOW + "  scp -pr %s %s@%s:~/" % (ARGS.file, USERNAME, privip) + RESET)
            if ARGS.command:
                if ARGS.out:
                    OUTFILE.write("  ssh %s '%s'\n" % (privip, ARGS.command))
                else:
                    print(YELLOW + "  ssh %s '%s'" % (privip, ARGS.command) + RESET)
            if ARGS.execute:
                if ARGS.out:
                    OUTFILE.write("  ssh %s 'sudo ./%s %s %s'\n" % (str(privip), ARGS.file, env, name))
                else:
                    print(YELLOW + "  ssh %s 'sudo ./%s %s %s'" % (str(privip), ARGS.file, env, name) + RESET)
            elif ARGS.execute and not ARGS.file:
                print(RED + "You have to specify which file to execute.  If it's already there, use -c" + RESET)
        else:
            if ARGS.out:
                OUTFILE.write("--- %s - %s ---\n" % (name, privip))
            else:
                print("--- %s - %s ---" % (name, privip))
            if ARGS.file:
                if ARGS.out:
                    OUTFILE.write(os.popen("scp -pr %s  %s@%s:~/" % (ARGS.file, USERNAME, privip)).read())
                else:
                    os.system("scp -pr %s  %s@%s:~/" % (ARGS.file, USERNAME, privip))
            if ARGS.command:
                if ARGS.out:
                    OUTFILE.write(os.popen("ssh %s '%s'\n" % (privip, ARGS.command)).read())
                else:
                    os.system("ssh %s '%s'\n" % (privip, ARGS.command))
            if ARGS.execute:
                if ARGS.out:
                    OUTFILE.write(os.popen("ssh %s 'sudo ./%s %s %s'" % (str(privip), ARGS.file, env, name)).read())
                else:
                    os.system("ssh %s 'sudo ./%s %s %s'" % (str(privip), ARGS.file, env, name))
            elif ARGS.execute and not ARGS.file:
                print(RED + "You have to specify which file to execute.  If it's already there, use -c" + RESET)

DIFF = round(time.time() - os.path.getmtime(DB_FILE))
if DIFF >= 3600:
    print(GREEN + "Database is older than 3600 seconds, rescanning..." + RESET)
    scaninstances()
elif ARGS.scan:
    print(GREEN + "Database is %s seconds old, but user requested a fresh scan, rescanning..." % (DIFF) + RESET)
    scaninstances()
else:
    print(GREEN + "Database is %s seconds old, proceeding with saved data..." % (DIFF) + RESET)

runscript()
DB.close()
